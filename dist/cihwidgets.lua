--
--  CIH Widgets.
--   - And stuff.
--
--[[
    Intended use: (CASE SENSITIVE)
        ParamIterator("Magic", createParamTable(127000, {
            RequiredINT = {Offset = 0x1E, Value = 0, Type = "Byte"},
            RequiredFAI = {Offset = 0x1F, Value = 0, Type = "Byte"}
            ... etc
        }), Name)
]]--
    function createParamTable (ID, Params)
        local ParamTable = {}
        local ParamTypes = {
            Float    = function(Param) return igro:floatToByteArray(Param.Value) end,
            FourByte = function(Param) return igro:decToByteArray(Param.Value, 4) end,
            TwoByte  = function(Param) return igro:decToByteArray(Param.Value, 2) end,
            Byte     = function(Param) return igro:decToByteArray(Param.Value, 1) end,
            Binary   = function(Param) return igro:decToByteArray(Param.Value, 1) end,
            Default  = function(Param) return igro:decToByteArray(Param.Value, Param.Size) end
        }
        for Name, Param in pairs(Params) do
            local Value = 0
            ParamTable[#ParamTable + 1] = {
                ID,
                Param.Offset,
                Param.Type ~= nil and ParamTypes[Param.Type](Param) or ParamTypes["Default"](Param)
            }
        end
        return ParamTable
    end
--[[
    Intended use: (SYNTAX SENSITIVE, WIP)
--      With generated names:
        local ParamTableNames = ParamTableIterator({
            {
                Type = "Magic",
                Target = 127000,
                Params = {
                    RequiredINT = {Offset = 0x1E, Value = 0,       Type = "Byte"},
                    RequiredFAI = {Offset = 0x1F, Value = 0,       Type = "Byte"},
                    Bullet1     = {Offset = 0x64, Value = 1000,    Type = "FourByte"}
                }
            },
            {
                Type = "Bullet",
                Target = 1000,
                Params = {
                    RecordLife  = {Offset = 0x58, Value = 0.00001, Type = "Float"},
                }
            }
        })
        ParamTableDepatcher(ParamTableNames)

--      With self-determined names:
        ParamTableIterator({
            {
                Type = "Magic",
                Name = "BobMagic",
                Target = 127000,
                Params = {
                    RequiredINT = {Offset = 0x1E, Value = 0,       Type = "Byte"},
                    RequiredFAI = {Offset = 0x1F, Value = 0,       Type = "Byte"},
                    Bullet1     = {Offset = 0x64, Value = 1000,    Type = "FourByte"}
                }
            },
            {
                Type = "Bullet",
                Name = "BobBullet",
                Target = 1000,
                Params = {
                    RecordLife  = {Offset = 0x58, Value = 0.00001, Type = "Float"},
                }
            }
        })
        ParamTableDepatcher({"BobMagic", "BobBullet"})
]]--
    IteratedParamTables = {}
    function ParamTableName (Name)
        local UUID = 0
        for i, d in pairs(IteratedParamTables) do
            if d.Type == Name or d.Name == Name then UUID = UUID + 1 end
        end
        return "ParamTable__" .. Name .. "__" .. UUID
    end
    function ParamTableIterator (ParamTables)
        local NextType
        local Names = {}
        local function Iterator(Type,ParamTable,ID)
            if ParamIterator then
                return ParamIterator(Type,ParamTable,ID)
            elseif paramUtils then
                return paramUtils:paramIterator(Type,ParamTable,ID)
            end
            return false
        end
        for i = 1, #ParamTables, 1 do
            local Key = ParamTables[i]
            local ParamTable = {
                ID = ParamTableName(Key.Name or Key.Type),
                ParamTable = createParamTable(Key.Target, Key.Params),
                Type = Key.Type,
                Target = Key.Target
            }
            ParamTable.Name = Key.Name or ParamTable.ID
            if IteratedParamTables[ParamTable.ID] ~= nil then
                error("ParamTable by ID [ " .. ParamTable.ID .. " ] already exists!")
            else
                IteratedParamTables[ParamTable.ID] = ParamTable
                Iterator(ParamTable.Type, ParamTable.ParamTable, ParamTable.ID)
                Names[#Names+1] = ParamTable.Name
            end
        end
        return Names
    end
    function ParamTableDepatcher (NameList)
        local function Depatcher(ID)
            if ParamDepatcher then
                ParamDepatcher(ID)
            elseif paramUtils then
                paramUtils:paramDepatcher(ID)
            end
            return false
        end
        for ptI, pt in pairs(IteratedParamTables) do
            for i, d in pairs(NameList) do
                if pt.Name == d then
                    Depatcher(pt.ID)
                    IteratedParamTables[ptI] = nil
                end
            end
        end
    end
--
--
--
--